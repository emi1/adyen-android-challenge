package org.bvr.androidadyenchallenge;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bvr on 12/21/16.
 */

public class RestaurantListAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    List<Restaurant> restaurants = new ArrayList<>();

    public RestaurantListAdapter(Activity activity, List<Restaurant> restaurants) {
        this.restaurants = restaurants;

        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return restaurants.size();
    }

    @Override
    public Object getItem(int position) {
        return restaurants.get(position);
    }

    @Override
    public long getItemId(int position) {
        //FIXME implement real id
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Restaurant currentObject = restaurants.get(position);

        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_restaurant, null);
        }

        TextView textViewName = (TextView) view.findViewById(R.id.restaurant_name);
        textViewName.setText(currentObject.getName());

        return view;
    }
}
