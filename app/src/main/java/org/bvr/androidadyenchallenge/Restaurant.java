package org.bvr.androidadyenchallenge;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bvr on 12/21/16.
 */
public class Restaurant {

    private String name;
    private int usersCount;

    private Restaurant() {

    }

    public static Restaurant fromJson(JSONObject jsonRestaurant) throws JSONException {
        Restaurant result = new Restaurant();
        result.name = jsonRestaurant.getString("name");
        result.usersCount = jsonRestaurant.getJSONObject("stats").getInt("usersCount");
        return result;
    }

    public String getName() {
        return name;
    }

    public int getUsersCount() {
        return usersCount;
    }
}
