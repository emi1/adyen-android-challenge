package org.bvr.androidadyenchallenge;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bvr on 12/21/16.
 */

public class FourSquareClient {

    private static final String secret = "VJX1RIDWYK3SYLCYGQCBM3YPJ5TUJ1YFDEZGSDOIYCTMRDKW";
    private static final String client_id = "EXG1Z0OLR1PSGACBUJKLWRJ4NANAD3PNAVN3EVIWOJ5TOENM";
    private static final String version_parameter = "20161222";
    private static final int limit = 50;
    private static final String categoryId_food = "4d4b7105d754a06374d81259";
    private static final String categoryId_restaurant = "4bf58dd8d48988d1c4941735";
    private static final int radius = 1000;
    private static final String intent = "browse";

    private static final String endPoint = "https://api.foursquare.com/v2/venues/search";

    public static String getRestaurants(double latitude, double longtitude) {

        final String request = endPoint
                + "/?ll=" + latitude + "," + longtitude
                + "&client_id=" + client_id
                + "&client_secret=" + secret
                + "&v=" + version_parameter
                + "&categoryId=" + categoryId_restaurant
                + "&limit=" + limit
                + "&radius=" + radius
                + "&intent=" + intent;

        URL url = null;
        try {
            url = new URL(request);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        StringBuilder stringBuilder = new StringBuilder();

        try {

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            urlConnection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }
}
