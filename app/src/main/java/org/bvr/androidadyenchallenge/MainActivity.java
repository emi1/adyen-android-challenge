package org.bvr.androidadyenchallenge;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout refreshLayout;

    public static final int ASK_MULTIPLE_PERMISSIONS_REQUEST_CODE = 23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //check and request permissions on Android 6.0
        checkPermissions();

        //refresh layout to refresh list via ui
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new DisplayRestaurantsASyncTask().execute();
            }
        });

        new DisplayRestaurantsASyncTask().execute();
    }

    /**
     * Android 6.0 (Marshmallow) requires to request of permissions from user at runtime
     */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissionsToRequest = new ArrayList<>();

            try {
                List<String> permissions = Arrays.asList(getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_PERMISSIONS).requestedPermissions);
                for (String perm : permissions) {
                    if (this.checkSelfPermission(perm) != PackageManager.PERMISSION_GRANTED) {
                        permissionsToRequest.add(perm);
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (permissionsToRequest.size() > 0) {
                String[] perms = new String[permissionsToRequest.size()];
                perms = permissionsToRequest.toArray(perms);
                requestPermissions(perms, ASK_MULTIPLE_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    /**
     * ASyncTask to retrieve restaurants from Froursquare and show it in ui
     * The 10 most popular restaurants are displayed in the list.
     * Restaurant A is considered more popular than restaurant B if the usersCount (number of
     * users who have ever checked in) is higher.
     */
    class DisplayRestaurantsASyncTask extends AsyncTask<Void, Void, Void> {

        private Activity activity;
        private List<Restaurant> restaurants = new ArrayList<>();

        private static final String restaurants_cache_key = "restaurants_cache_key";

        /**
         * Type for gson serialization of restaurant list
         */
        private final Type gsonType = new TypeToken<ArrayList<Restaurant>>() {}.getType();

        public DisplayRestaurantsASyncTask() {
            this.activity = MainActivity.this;
        }

        @Override
        protected Void doInBackground(Void... params) {

            //Check if internet connectivity exists, if not restore from cache
            if (Utils.isInternetConnected(activity)) {

                Location location = getLocation();

                if (location == null) {
                    Utils.toast(activity, "Could not get location.");
                    return null;
                }

                //get all restaurants in neighbourhood
                String restaurantsStr = FourSquareClient.getRestaurants(location.getLatitude(), location.getLongitude());

                try {
                    //Get JsonArray with venus
                    JSONArray restaurantsJsonArray = new JSONObject(restaurantsStr)
                            .getJSONObject("response")
                            .getJSONArray("venues");
                    //Convert JsonArray to list
                    for (int i = 0; i < restaurantsJsonArray.length(); i++) {
                        restaurants.add(Restaurant.fromJson(restaurantsJsonArray.getJSONObject(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Sort restaurants by usersCount (total users who have ever checked in)
                Collections.sort(restaurants, new Comparator<Restaurant>() {
                    @Override
                    public int compare(Restaurant lhs, Restaurant rhs) {
                        return (-1) * Integer.valueOf(lhs.getUsersCount()).compareTo(rhs.getUsersCount());
                    }
                });

                //Trimming list, use only 10 most popular entries
                if (restaurants.size() < 10) {
                    restaurants = restaurants.subList(0, restaurants.size());
                } else {
                    restaurants = restaurants.subList(0, 10);
                }

                Utils.toast(activity, "Retrieved list from Foursquare.");

                //Write list to shared preferences
                String cache = new Gson().toJson(restaurants, gsonType);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                prefs.edit()
                        .putString(restaurants_cache_key, cache)
                        .apply();

            } else {
                //retrieve cached list if no connection to internet exists
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                String fromCache = prefs.getString(restaurants_cache_key, "");
                if (fromCache.isEmpty()) {
                    restaurants = new ArrayList<>();
                    Utils.toast(activity, "No internet and no cache available.");
                } else {
                    restaurants = new Gson().fromJson(fromCache, gsonType);
                    Utils.toast(activity, "List restored from cache.");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            ListView restaurantsListView = (ListView) activity.findViewById(R.id.list_view_restaurants);

            //display data in list
            RestaurantListAdapter adapter = new RestaurantListAdapter(activity, restaurants);
            restaurantsListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            refreshLayout.setRefreshing(false);

            super.onPostExecute(aVoid);
        }

        /**
         * Returns the last known location of the user using the best available provider (GPS, network, ...)
         * @return last known location
         * @throws SecurityException Exception is thrown if we don't have location permissions
         */
        private Location getLocation() throws SecurityException{
            LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
            //Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            List<String> providers = locationManager.getProviders(true);
            Location bestLocation = null;
            //iterate over all location providers and use the one with the most accuracy
            for (String provider : providers) {
                Location location = locationManager.getLastKnownLocation(provider);
                if (location == null) {
                    continue;
                }
                if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = location;
                }
            }
            return bestLocation;
        }

    }

}
